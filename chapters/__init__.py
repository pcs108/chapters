import json
import typing
from dataclasses import dataclass
from pathlib import Path

from ffmpeg_timestamp import TimeStamp
from mutagen.mp4 import MP4
from run_command import run_command
from tagger import FileTagger


@dataclass(slots=True)
class CrackedAudibleTagsParser:
    path: Path

    # Cache results.
    _tag_reader: typing.Optional[MP4] = None
    _tags: typing.Optional[dict] = None

    @property
    def tag_reader(self) -> MP4:
        self._tag_reader = self._tag_reader or MP4(self.path)
        return self._tag_reader

    @property
    def tags(self) -> dict:
        self._tags = dict(self._tags or self.tag_reader.tags or {})
        return self._tags

    def parse(self) -> typing.Dict[str, str]:
        return {
            "title": self.tags["©nam"],
            "artist": self.tags["©ART"],
            "album": self.tags["©alb"],
            "genre": "Audiobook",
        }


@dataclass(slots=True, kw_only=True)
class Chapter:
    SPLIT_CHAPTER_COMMAND = (
        "ffmpeg -i {original_file!r} -ss {start} -to {end} {output_file!r}"
    )

    original_file: Path
    output_file: Path
    start: typing.Union[TimeStamp, str]
    end: typing.Union[TimeStamp, str]
    tags: typing.Dict[str, typing.Union[str, int]]

    # Cache results
    _tagger: typing.Optional[FileTagger] = None

    @property
    def tagger(self) -> FileTagger:
        self._tagger = self._tagger or FileTagger.from_file(self.output_file)
        return self._tagger

    def split_and_tag(self) -> None:
        cmd = Chapter.SPLIT_CHAPTER_COMMAND.format(
            original_file=str(self.original_file),
            start=str(self.start),
            end=str(self.end),
            output_file=str(self.output_file),
        )
        run_command(cmd)
        self.tagger.tag(self.tags)


@dataclass(slots=True)
class ChapterInfoParser:
    FIND_CHAPTERS_COMMAND = (
        "ffprobe -v quiet -print_format json -show_chapters {path!r}"
    )

    path: Path

    def parse(self) -> typing.List[dict]:
        cmd = ChapterInfoParser.FIND_CHAPTERS_COMMAND.format(path=str(self.path))
        result = run_command(cmd, capture_output=True, check=True)
        if not result:
            raise ValueError(cmd)
        return json.loads(result)["chapters"]


@dataclass
class ChapterBuilder:
    original_file: Path
    original_file_tags: dict
    track_number: int
    total_tracks: int
    chapter_data: dict

    @property
    def start(self) -> TimeStamp:
        return TimeStamp.from_value(self.chapter_data["start_time"])

    @property
    def end(self) -> TimeStamp:
        return TimeStamp.from_value(self.chapter_data["end_time"])

    @property
    def output_file(self) -> Path:
        return self.original_file.parent / f"{self.title}.mp3"

    @property
    def title(self) -> str:
        return f"{self.original_file.stem} - Chapter {self.track_number}"

    @property
    def tags(self) -> dict:
        return {
            "title": self.title,
            "artist": self.original_file_tags["artist"],
            "album": self.original_file_tags["album"],
            "track_number": self.track_number,
            "total_tracks": self.total_tracks,
            "genre": self.original_file_tags["genre"],
        }

    def build(self) -> Chapter:
        return Chapter(
            original_file=self.original_file,
            output_file=self.output_file,
            start=self.start,
            end=self.end,
            tags=self.tags,
        )


@dataclass(slots=True)
class ChapterCollectionBuilder:
    FIND_CHAPTERS_COMMAND = (
        "ffprobe -i {original_file!r} -print_format json -show_chapters"
    )

    original_file: Path

    # Cache results
    _chapter_info_parser: typing.Optional[ChapterInfoParser] = None
    _original_file_tags_parser: typing.Optional[CrackedAudibleTagsParser] = None

    @property
    def chapter_info_parser(self) -> ChapterInfoParser:
        self._chapter_info_parser = self._chapter_info_parser or ChapterInfoParser(
            self.original_file
        )
        return self._chapter_info_parser

    @property
    def original_file_tags_parser(self) -> CrackedAudibleTagsParser:
        self._original_file_tags_parser = (
            self._original_file_tags_parser
            or CrackedAudibleTagsParser(self.original_file)
        )
        return self._original_file_tags_parser

    def build(self) -> typing.List[Chapter]:
        results = []

        original_file_tags = self.original_file_tags_parser.parse()
        chapters_info = self.chapter_info_parser.parse()
        total_tracks = len(chapters_info)
        for track_number, chapter_data in enumerate(chapters_info, 1):
            chapter = ChapterBuilder(
                original_file=self.original_file,
                original_file_tags=original_file_tags,
                track_number=track_number,
                total_tracks=total_tracks,
                chapter_data=chapter_data,
            ).build()
            results.append(chapter)
        return results
