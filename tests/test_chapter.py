import dataclasses
from pathlib import Path

from chapters import Chapter
from ffmpeg_timestamp import TimeStamp


@dataclasses.dataclass
class FakeFileTagger:
    applied_tags: dict = dataclasses.field(default_factory=dict)

    def tag(self, tags: dict) -> None:
        self.applied_tags = {**self.applied_tags, **tags}


def test_chapter_split_and_tag_splits_file_and_applies_tags(run_command_faker):
    original_file = Path("/fake.mp4")
    output_file = Path("/fake-chapter-002.mp3")
    tags = {
        "title": "Fake Book - Chapter 2",
        "author": "Fake Author",
        "album": "Fake Book",
        "tracknumber": 2,
        "total_tracks": 108,
        "genre": "Audiobook",
    }
    fake_file_tagger = FakeFileTagger()
    chapter = Chapter(
        original_file=original_file,
        output_file=output_file,
        start=TimeStamp.from_value("00:01:02"),
        end=TimeStamp.from_value("00:12:34"),
        tags=tags,
        _tagger=fake_file_tagger,
    )
    chapter.split_and_tag()

    cmd = Chapter.SPLIT_CHAPTER_COMMAND.format(
        original_file=str(original_file),
        start="00:01:02",
        end="00:12:34",
        output_file=str(output_file),
    )
    run_command_faker.mock.assert_called_with(cmd, shell=True, capture_output=False)
    assert fake_file_tagger.applied_tags == tags
