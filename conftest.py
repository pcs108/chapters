import pytest


@pytest.fixture(autouse=True)
def always_fake_commands(run_command_faker):
    return run_command_faker
