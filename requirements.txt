black==23.11.0
click==8.1.7
flake8==6.1.0
isort==5.12.0
mutagen==1.47.0
pytest==7.4.3
pytest-mock==3.12.0
ffmpeg-timestamp @ git+https://gitlab.com/pcs108/ffmpeg-timestamp.git@e21c47ad0cda4def162ad4935e8d9ffa92a7da7d
run-command @ git+https://gitlab.com/pcs108/run-command.git@341dd3b71eb95e4a943f144387a36146e05b61b6
tagger @ git+https://gitlab.com/pcs108/tagger.git@45efbc2264dd49d57a7b16e7f1383c9cf1807b74
