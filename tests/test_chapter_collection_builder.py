from dataclasses import dataclass
from pathlib import Path
from typing import Any

from chapters import Chapter, ChapterCollectionBuilder


@dataclass(slots=True)
class FakeParser:
    results: Any

    def parse(self) -> Any:
        return self.results


def test_chapter_collection_builder_builds_chapters():
    chapter_info = [
        {"start_time": "0.0000", "end_time": "1234.567"},
        {"start_time": "1235.0", "end_time": "1236.23"},
    ]
    tags = {"artist": "Fake Author", "album": "Fake Book", "genre": "Audiobook"}

    original_file = Path("/fake.mp4")
    chapters = ChapterCollectionBuilder(
        original_file,
        _chapter_info_parser=FakeParser(chapter_info),
        _original_file_tags_parser=FakeParser(tags),
    ).build()

    assert chapters == [
        Chapter(
            original_file=original_file,
            output_file=Path("/fake - Chapter 1.mp3"),
            start="00:00:00.0000",
            end="00:20:34.567",
            tags={
                "title": "fake - Chapter 1",
                "artist": "Fake Author",
                "album": "Fake Book",
                "track_number": 1,
                "total_tracks": 2,
                "genre": "Audiobook",
            },
        ),
        Chapter(
            original_file=original_file,
            output_file=Path("/fake - Chapter 2.mp3"),
            start="00:20:35.0",
            end="00:20:36.23",
            tags={
                "title": "fake - Chapter 2",
                "artist": "Fake Author",
                "album": "Fake Book",
                "track_number": 2,
                "total_tracks": 2,
                "genre": "Audiobook",
            },
        ),
    ]
