from pathlib import Path

import click

from . import ChapterCollectionBuilder


@click.command()
@click.argument("path")
@click.option("-f", "--force", is_flag=True)
def chapters(path: str, force: bool) -> None:
    original = Path(path)
    chapters = ChapterCollectionBuilder(original).build()
    for chapter in chapters:
        chapter.split_and_tag()

    if force:
        original.unlink()


def main() -> None:
    chapters()


if __name__ == "__main__":
    main()
