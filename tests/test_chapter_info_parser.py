import json
from pathlib import Path

from chapters import ChapterInfoParser

CHAPTERS = """[
        {
            "id": 0,
            "time_base": "1/1000",
            "start": 0,
            "start_time": "0.000000",
            "end": 2706146,
            "end_time": "2706.146000",
            "tags": {
                "title": "Chapter 1"
            }
        },
        {
            "id": 1,
            "time_base": "1/1000",
            "start": 2706146,
            "start_time": "2706.146000",
            "end": 5363206,
            "end_time": "5363.206000",
            "tags": {
                "title": "Chapter 2"
            }
        },
        {
            "id": 2,
            "time_base": "1/1000",
            "start": 5363206,
            "start_time": "5363.206000",
            "end": 7870729,
            "end_time": "7870.729000",
            "tags": {
                "title": "Chapter 3"
            }
        }
    ]
"""

STDOUT = f"""{{
    "chapters": {CHAPTERS}
}}
"""


def test_chapter_info_parser_parses_chapter_info_from_file(run_command_faker):
    run_command_faker.set_result(stdout=STDOUT)

    path = Path("/fake.mp4")
    assert ChapterInfoParser(path).parse() == json.loads(CHAPTERS)

    command = ChapterInfoParser.FIND_CHAPTERS_COMMAND.format(path=str(path))
    run_command_faker.mock.assert_called_with(
        command, shell=True, check=True, capture_output=True
    )
