import typing
from dataclasses import dataclass
from pathlib import Path

from chapters import CrackedAudibleTagsParser


@dataclass
class FakeMutagenMP4:
    @property
    def tags(self) -> typing.Dict[str, str]:
        return {"©nam": "title", "©ART": "artist", "©alb": "album"}


def test_cracked_audible_tags_parser_parses_regular_tags_from_audible_tags():
    path = Path("/fake.mp4")
    parser = CrackedAudibleTagsParser(path=path, _tag_reader=FakeMutagenMP4())
    assert parser.parse() == {
        "title": "title",
        "artist": "artist",
        "album": "album",
        "genre": "Audiobook",
    }
