from pathlib import Path

from chapters import Chapter, ChapterBuilder


def test_chapter_builder_builds_appropriate_chapter():
    original_file = Path("/fake.mp4")
    original_file_tags = {
        "artist": "Fake Author",
        "album": "Fake Book",
        "genre": "Freaky",
    }
    track_number = 2
    total_tracks = 108
    chapter_data = {"start_time": "123", "end_time": "456"}

    chapter = ChapterBuilder(
        original_file=original_file,
        original_file_tags=original_file_tags,
        track_number=track_number,
        total_tracks=total_tracks,
        chapter_data=chapter_data,
    ).build()
    assert chapter == Chapter(
        original_file=original_file,
        output_file=Path("/fake - Chapter 2.mp3"),
        start="00:02:03",
        end="00:07:36",
        tags={
            "title": "fake - Chapter 2",
            "artist": "Fake Author",
            "album": "Fake Book",
            "track_number": 2,
            "total_tracks": 108,
            "genre": "Freaky",
        },
    )
